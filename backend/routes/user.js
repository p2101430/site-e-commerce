import express from 'express'
import { User, Product, Bid } from '../orm/index.js'

const router = express.Router()

router.get('/api/users/:userId', async (req, res) => {

  try{

    const findUser = await User.findOne({where: {"id": req.params.userId} , include: [{
      model: Product,
      as: 'products'
    },
    {
      model: Bid,
      as: 'bids'
    }]
    })

    const findProduct = await Product.findAll({where: {"sellerId": req.params.userId}})

    // const findProduct = await Product.findAll({where: {"sellerId": req.params.userId} , include: [{
    //   model: Product,
    //   required: true
    // }]})

    // for(let bid of findProduct){
    //   bid.setDataValue.setDataValue({"product": [{'id': findProduct.id , "username": findUser.username}]})
    // }

    for(let i=0; i < findProduct.length; i++){
      findUser.bids[i].setDataValue({"product": [{'id': findProduct.id , "username": findUser.username}]})
    }
    res.status(200).send(findUser)
  }catch(error){
    res.status(404).send("user not found")
  }

})

export default router
