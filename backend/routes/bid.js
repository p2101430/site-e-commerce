import authMiddleware from '../middlewares/auth.js'
import { Bid, Product  , User} from '../orm/index.js'
import express from 'express'
import { getDetails } from '../validators/index.js'
import bid from '../orm/models/bid.js'

const router = express.Router()

router.delete('/api/bids/:bidId', authMiddleware , async (req, res) => {

  bid.destroy({where:{"id": req.params.bidId}})
  res.status(204).send("no content")
})

router.post('/api/products/:productId/bids', authMiddleware, async (req, res) => {
  try {
    const newBid = req.body
    res.status(201).send(newBid)
  } catch (error) {
    switch(error){
      case 400:
        res.status(400).send({
          "error": "Invalid or missing fields",
          "details": ["name", "endDate"]
        });
      break;
      case 401:
          res.status(401).send({"error": "Unauthorized"});
        break;
      case 403:
          res.status(403).send({"error": "User not granted"});
        break;
      case 404:
          res.status(404).send({"error": "Product not found"});
        break;
    }
  }
})

export default router
