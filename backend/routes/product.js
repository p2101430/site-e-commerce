import express from 'express'
import { Product, Bid, User } from '../orm/index.js'
import authMiddleware from '../middlewares/auth.js'
import { getDetails } from '../validators/index.js'
import { where } from 'sequelize'
import user from '../orm/models/user.js'

const router = express.Router()

router.get('/api/products', async (req, res, next) => {
  const products = await Product.findAll({include: [{
    model: User,
    as: 'seller',
    attributes: ['id' , 'username']
  },
  {
    model: Bid,
    as: 'bids',
    attributes: ['id' , 'price' , 'date']
  }
]});
  res.status(200).send(products);
})

router.get('/api/products/:productId', async (req, res) => {

  try {
    const findProduct = await Product.findOne({where: {"id": req.params.productId} , include: [{
      model: User,
      as: 'seller',
      attributes: ['id' , 'username']
    },
    {
      model: Bid,
      as: 'bids'
    }]
    })

    const findBidder = await User.findAll()

    // let bidderFinded = "";

    for(let i = 0; i < findBidder.length; i++){
      findProduct.bids[i].setDataValue("bidder", [{'id': findBidder[i].id , "username": findBidder[i].username}])
    }

    // for(let i = 1; i < findBidder.length; i++){
    //   if(findBidder.get(i).id == findProduct.Bid.bidderId){
    //     bidderFinded = findBidder[i]
    //   }
    //   // else{
    //   //   res.status(404).send()
    //   // }
    // }

    // findProduct.setDataValue("bidder", [{'id': bidderFinded.id,"username": bidderFinded.username}])
    
    res.status(200).send(findProduct)
  } catch (error) {
    res.status(404).send()
  }
})

// You can use the authMiddleware with req.user.id to authenticate your endpoint ;)

router.post('/api/products', authMiddleware, async (req, res) => {
  let request = req.body
  request['sellerId'] = req.user.id
  try {
    res.status(201).send(await Product.create(request));
  } catch (error) {
    res.status(400).send({"error" : "Invalid or missing fields", "details" : error});
  }
})

router.put('/api/products/:productId', authMiddleware , async (req, res) => {

    try {
      const findProduct = await Product.findAll({where: {"id": req.params.productId}})

      if(findProduct){
        if(authMiddleware){
          const updateProduct = req.body
          res.status(200).send(updateProduct)
        }
        else{
          res.status(403).send({"error": "User not granted"});
        }
      }
      else{
        res.status(404).send({"error": "Product not found"});
      }

    } catch (error) {

      switch(error){
        case 400:
            res.status(400).send({
              "error": "Invalid or missing fields",
              "details": ["name", "endDate"]
            });
          break;
        case 401:
            res.status(401).send({"error": "Unauthorized"});
          break;
      }
    }
})

router.delete('/api/products/:productId', authMiddleware, async (req, res) => {
  try {
    Product.destroy({where: {"id": req.params.productId}})
    res.status(204).send("No Content")

  } catch (error) {
    switch(error){
      case 401:
          res.status(401).send({"error": "Unauthorized"});
        break;
      case 403:
          res.status(403).send({"error": "User not granted"});
        break;
      case 404:
          res.status(404).send({"error": "Product not found"});
        break;
    }
  }
})

export default router